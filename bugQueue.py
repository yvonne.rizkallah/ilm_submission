"""
Created By: Yvonne Rizkallah
Created Date: Jan 2022
version: 1.0

bugQueue.py: A priority queue that receives an incoming dict with two keys, a priority and a command
             Items of the same priority are processed in the order they are received
"""

class BugQueue():
    def __init__(self):
        self.queue = []

    def insert(self, dict):
        p, c = (list(dict.values()))
        if type(p) != int or not 0 <= p < 10:
            p = 10  # redefine priority if invalid
        o = len(self.queue)  # order added to queue
        self.queue.append([p, o, c])  # sort by prioriry, then order
        self.queue.sort()

    def get(self):
        return self.queue.pop(0)

    def empty(self):
        return len(self.queue) == 0

    def __str__(self):
        return ' '.join([str(i) for i in self.queue])
